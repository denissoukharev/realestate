// Get the property ID from the URL parameter
const urlParams = new URLSearchParams(window.location.search);
const propertyId = urlParams.get("id");

// Fetch XML data
fetch("https://portal.apinmo.com/xml/xml2demo/2-web.xml")
  .then((response) => response.text())
  .then((data) => {
    // Parse XML data
    const parser = new DOMParser();
    const xmlData = parser.parseFromString(data, "text/xml");
    const idValue = propertyId;
    const xpathQuery = '//propiedad[id="' + idValue + '"]';
    const matchingElements = xmlData.evaluate(
      xpathQuery,
      xmlData,
      null,
      XPathResult.ANY_TYPE,
      null
    );

    // Retrieve the first matching element
    const property = matchingElements.iterateNext();
    console.log(property);
    if (property) {
      // Get the property details
      const title = property.getElementsByTagName("titulo1")[0].textContent;
      const descrip1 = property.getElementsByTagName("descrip1")[0].textContent;
      //const foto = property.getElementsByTagName("foto1")[0].textContent;
      //   const price = property.getElementsByTagName("precio")[0].textContent;

      // Display the property details
      const propertyDetails = document.getElementById("propertyDetails");
      const propertyElement = document.createElement("div");
      propertyElement.innerHTML =
        "<h2>" + title + "</h2>" + "<p>" + descrip1 + "</p>";
      propertyDetails.appendChild(propertyElement);
    } else {
      // Property not found
      const propertyDetails = document.getElementById("propertyDetails");
      propertyDetails.innerHTML = "<p>Property not found.</p>";
    }
  })
  .catch((error) => console.error(error));
